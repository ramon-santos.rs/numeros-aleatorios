#!/bin/bash
clear

for x in {1..1000};
do
	echo $x
done

for ((x=0; x<10; x++))
do
	echo $((x*x))
done

for x in a b c d e f;
do
	echo $x > $x
done

for x in $*;
do 
	echo $x
done

for n in {0..100}
do
	echo "n + 1 = $((n + 1))"
done	
